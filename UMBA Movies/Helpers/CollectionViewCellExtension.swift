//
//  CollectionViewCellExtension.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

extension UICollectionViewCell {
    class func reusableIdentifier() -> String {
        let classType: AnyClass = object_getClass(self)!
        return NSStringFromClass(classType)
    }
}
