//
//  CGFloatExtension.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

extension CGFloat {
    
    static var padding: CGFloat {
        return 16.0
    }
    
    static var inversePadding: CGFloat {
        return -16.0
    }
    
    static var buttonWidth: CGFloat {
        return 40.0
    }
}
