//
//  StringExtension.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

extension String {
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(height: CGFloat, font: UIFont) -> CGFloat{
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    // Keys
    static let kLatest: String = "com.UMBA.Movies.Keys.latestMovies"
    static let kPopular: String = "com.UMBA.Movies.Keys.popularMovies"
    static let kUpcoming: String = "com.UMBA.Movies.Keys.upcomingMovies"
}
