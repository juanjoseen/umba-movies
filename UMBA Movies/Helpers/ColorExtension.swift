//
//  ColorExtension.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

extension UIColor {
    static var primary: UIColor {
        get {
            return UIColor(named: "backgroundColor")!
        }
    }
    
    static var secondary: UIColor {
        get {
            return UIColor(red: 1/255, green: 180/255, blue: 228/255, alpha: 1.0)
        }
    }
    
    static var tertiary: UIColor {
        get {
            return UIColor(red: 144/255, green: 206/255, blue: 161/255, alpha: 1.0)
        }
    }
    
    static var textColor: UIColor {
        get {
            return UIColor(named: "textColor")!
        }
    }
}
