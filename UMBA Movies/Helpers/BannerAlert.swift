//
//  BannerAlert.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

protocol BannerAlertDelegate {
    func showAlert()
    func hideAlert(animated: Bool)
}

enum AlertType {
    case error
    case warning
    case success
    case info
}

class BannerAlert: UIView {
    
    private weak var lblMessage: UILabel?
    
    var delegate: BannerAlertDelegate?

    var message: String {
        didSet {
            lblMessage?.text = message
        }
    }
    
    var type: AlertType {
        didSet {
            switch type {
            case .error:
                backgroundColor = .systemRed
            case .warning:
                backgroundColor = .systemYellow
            case .success:
                backgroundColor = .systemGreen
            case .info:
                backgroundColor = .systemTeal
            }
        }
    }
    
    init(message: String, type: AlertType = .error) {
        self.message = message
        self.type = type
        super.init(frame: .zero)
        
        translatesAutoresizingMaskIntoConstraints = false
        
        let lblMessage: UILabel = UIFactory.getLabel(text: message, color: .textColor, font: .title(), alignment: .center)
        self.lblMessage = lblMessage
        
        switch type {
        case .error:
            backgroundColor = .systemRed
        case .warning:
            backgroundColor = .systemYellow
        case .success:
            backgroundColor = .systemGreen
        case .info:
            backgroundColor = .systemTeal
        }
        
        addSubview(lblMessage)
        
        NSLayoutConstraint.activate([
            lblMessage.topAnchor.constraint(equalTo: topAnchor),
            lblMessage.bottomAnchor.constraint(equalTo: bottomAnchor),
            lblMessage.leadingAnchor.constraint(equalTo: leadingAnchor),
            lblMessage.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
