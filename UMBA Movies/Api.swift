//
//  Api.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

class Api {
    private init() {}
    
    static let shared: Api = Api()
    private let locale = Locale.preferredLanguages[0]
    private let urlSession: URLSession = URLSession.shared
    private let API_KEY: String = "bbb4a2023a3089d092b22087cb5817fc"
    private let ACCESS_TOKEN: String = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiYmI0YTIwMjNhMzA4OWQwOTJiMjIwODdjYjU4MTdmYyIsInN1YiI6IjYxMjg1NDUxMGQ5ZjVhMDAyYzUxNTVmYSIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.zbQDqWAT2wHm75Vl7nWoPD3nZwK_sEVc1O4gb2v0S9c"
    
    func getLatest(page: Int = 1, completion: @escaping (MovieResult?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/now_playing?api_key=\(API_KEY)&page=\(page)&language=\(locale)")!
        
        self.fetch(url: url) { movies in
            completion(movies)
        }
    }
    
    func getPopularMovies(page: Int = 1, completion: @escaping (MovieResult?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/popular?api_key=\(API_KEY)&page=\(page)&language=\(locale)")!
        
        fetch(url: url) { movies in
            completion(movies)
        }
    }
    
    func getUpcomingMovies(page: Int = 1, completion: @escaping (MovieResult?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/upcoming?api_key=\(API_KEY)&page=\(page)&language=\(locale)")!
        
        fetch(url: url) { movies in
            completion(movies)
        }
    }
    
    func getSimilar(id: Int, completion: @escaping (MovieResult?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/\(id)/similar?api_key=\(API_KEY)&language=\(locale)")!
        
        fetch(url: url) { movies in
            completion(movies)
        }
    }
    
    func getDetailsOfMovie(id: Int, completion: @escaping (Movie?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/\(id)?api_key=\(API_KEY)&language=\(locale)")!
        
        fetch(url: url) { movie in
            completion(movie)
        }
    }
    
    func getVideoOfMovie(id: Int, completion: @escaping (Video?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/movie/\(id)/videos?api_key=\(API_KEY)&language=\(locale)")!
        
        fetch(url: url) { video in
            completion(video)
        }
    }
    
    func getCast(id: Int, fromMovie: Bool = true, completion: @escaping (Credits?) -> Void) {
        let url: URL = URL(string: String(format: "https://api.themoviedb.org/3/%@/%d/credits?api_key=%@&language=%@", fromMovie ? "movie" : "tv", id, API_KEY, locale))!
        
        fetch(url: url) { cast in
            completion(cast)
        }
    }
    
    func getGenreList(completion: @escaping (GenreList?) -> Void) {
        let url: URL = URL(string: "https://api.themoviedb.org/3/genre/movie/list?api_key=\(API_KEY)&language=\(locale)")!
        
        fetch(url: url) { genres in
            completion(genres)
        }
    }
    
    private func fetch<T: Decodable>(url: URL, completion: @escaping (T?) -> Void) {
        self.urlSession.dataTask(with: url) { result in
            switch result {
            case .success(let (response, data)):
                do {
                    if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                        let result: T = try JSONDecoder().decode(T.self, from: data)
                        completion(result)
                    } else {
                        print("error")
                        completion(nil)
                    }
                } catch {
                    print(error)
                    completion(nil)
                }
            case .failure(let error):
                print(error)
                completion(nil)
            }
        }.resume()
    }
}


extension URLSession {
    
    func dataTask(with url:URL, result: @escaping (Result<(URLResponse, Data), Error>) -> Void) -> URLSessionDataTask {
        return dataTask(with: url) { (data, response, error) in
            if let error = error {
                result(.failure(error))
                return
            }
            
            guard let response = response, let data = data else {
                let error = NSError(domain: "error", code: 0, userInfo: nil)
                result(.failure(error))
                return
            }
            result(.success((response, data)))
        }
    }
}
