//
//  ViewController.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class ViewController: UIViewController {
    
    weak var mainView: VMain?
    
    var sections: [SectionModel] = []
    
    override func loadView() {
        let mainView: VMain = VMain(controller: self)
        self.mainView = mainView
        view = mainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        sections = [
            getLatestFromLocal(),
            getPopularFromLocal(),
            getUpcomingFromLocal()
        ]
        
        getDataFromInternet()
    }
    
    func showDetailsOf(_ model: Preview) {
        let detail: DetailViewController = DetailViewController(idMovie: model.id)
        
        navigationController?.pushViewController(detail, animated: true)
    }
    
    private func getDataFromInternet() {
        getLatestMoviesFromInternet()
        getPopularFromInternet()
        getUpcomingFromInternet()
    }
    
    private func getLatestFromLocal() -> SectionModel {
        let latest: SectionModel = SectionModel(name: "Latest Movies")
        latest.items = self.getLatestInLocal()
        return latest
    }
    
    private func getPopularFromLocal() -> SectionModel {
        let popular: SectionModel = SectionModel(name: "Popular Movies")
        popular.items = self.getPopularInLocal()
        return popular
    }
    
    private func getUpcomingFromLocal() -> SectionModel {
        let upcoming: SectionModel = SectionModel(name: "Upcoming Movies")
        upcoming.items = self.getUpcomingInLocal()
        return upcoming
    }
    
    private func getLatestMoviesFromInternet() {
        DispatchQueue.global(qos: .background).async {
            if let latest: SectionModel = self.sections.filter({ $0.name == "Latest Movies" }).first {
                Api.shared.getLatest { result in
                    if let result = result, result.results.count > 0 {
                        latest.items = result.results
                        self.saveLatestInLocal(result.results)
                    } else {
                        self.mainView?.showError("Error getting data from server".localized)
                    }
                    DispatchQueue.main.async {
                        self.mainView?.collection?.reloadData()
                    }
                }
            }
        }
    }
    
    private func getPopularFromInternet() {
        DispatchQueue.global(qos: .background).async {
            if let popular: SectionModel = self.sections.filter({ $0.name == "Popular Movies" }).first {
                Api.shared.getPopularMovies { result in
                    if let result = result, result.results.count > 0 {
                        popular.items = result.results
                        self.savePopularInLocal(result.results)
                    } else {
                        self.mainView?.showError("Error getting data from server".localized)
                    }
                    DispatchQueue.main.async {
                        self.mainView?.collection?.reloadData()
                    }
                }
            }
        }
    }
    
    private func getUpcomingFromInternet() {
        DispatchQueue.global(qos: .background).async {
            if let upcoming: SectionModel = self.sections.filter({ $0.name == "Upcoming Movies" }).first {
                Api.shared.getUpcomingMovies { result in
                    if let result = result, result.results.count > 0 {
                        upcoming.items = result.results
                        self.saveUpcomingInLocal(result.results)
                    } else {
                        self.mainView?.showError("Error getting data from server".localized)
                    }
                    DispatchQueue.main.async {
                        self.mainView?.collection?.reloadData()
                    }
                }
            }
        }
    }
    
    private func saveLatestInLocal(_ items: [MoviePreview]) {
        do {
            let data = try JSONEncoder().encode(items)
            UserDefaults.standard.set(data, forKey: .kLatest)
        } catch {
            print("Error saving data")
        }
    }
    
    private func getLatestInLocal() -> [MoviePreview] {
        var latest: [MoviePreview] = []
        do {
            if let data = UserDefaults.standard.data(forKey: .kLatest) {
                latest = try JSONDecoder().decode([MoviePreview].self, from: data)
            }
        } catch {
            print("Error getting data")
        }
        return latest
    }
    
    private func savePopularInLocal(_ items: [MoviePreview]) {
        do {
            let data = try JSONEncoder().encode(items)
            UserDefaults.standard.set(data, forKey: .kPopular)
        } catch {
            print("Error saving data")
        }
    }
    
    private func getPopularInLocal() -> [MoviePreview] {
        var popular: [MoviePreview] = []
        do {
            if let data = UserDefaults.standard.data(forKey: .kPopular) {
                popular = try JSONDecoder().decode([MoviePreview].self, from: data)
            }
        } catch {
            print("Error getting data")
        }
        return popular
    }
    
    private func saveUpcomingInLocal(_ items: [MoviePreview]) {
        do {
            let data = try JSONEncoder().encode(items)
            UserDefaults.standard.set(data, forKey: .kUpcoming)
        } catch {
            print("Error saving data")
        }
    }
    
    private func getUpcomingInLocal() -> [MoviePreview] {
        var upcoming: [MoviePreview] = []
        do {
            if let data = UserDefaults.standard.data(forKey: .kPopular) {
                upcoming = try JSONDecoder().decode([MoviePreview].self, from: data)
            }
        } catch {
            print("Error getting data")
        }
        return upcoming
    }
    
}

