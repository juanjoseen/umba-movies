//
//  DetailViewController.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailViewController: UIViewController {
    weak var detailView: DetailView?
    
    var idMovie: Int!
    var movie: Movie!
    var models: [DetailModel] = []
    var cast: [Cast] = []
    var videos: Video? = nil
    var similar: [MoviePreview] = []
    
    convenience init(idMovie: Int) {
        self.init()
        self.idMovie = idMovie
    }
    
    override func loadView() {
        let detailView: DetailView = DetailView(controller: self)
        self.detailView = detailView
        view = detailView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.detailView?.title = "Loading...".localized
        
        // Do any additional setup after loading the view.
        view.backgroundColor = .primary
        
        
        Api.shared.getDetailsOfMovie(id: idMovie) { result in
            if let movie = result {
                self.movie = movie
                self.fillModelsWith(movie)
                DispatchQueue.main.async {
                    self.detailView?.fillDataWith(movie)
                    self.detailView?.title = movie.title
                }
                DispatchQueue.global(qos: .background).async {
                    self.getCast()
                }
                DispatchQueue.global(qos: .background).async {
                    self.getVideos()
                }
                DispatchQueue.global(qos: .background).async {
                    self.getSimilar()
                }
            } else {
                self.detailView?.showError("Error getting details")
            }
        }
    }
    
    func goToHomepage() {
        let url: URL = URL(string: movie.homepage!)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    func goToVideos() {
        //coordinator?.eventOccurred(with: .videosTapped, data: videos)
    }
    
    func showDetailsOf(_ model: MoviePreview) {
        //coordinator?.eventOccurred(with: .movieTapped, data: model)
    }
    
    private func fillModelsWith(_ movie: Movie) {
        models = [
            DetailHeaderModel(with: movie),
            DetailOverviewModel(with: movie),
            DetailMoreInfoModel(with: movie)
        ]
        
        if movie.homepage != nil || self.videos != nil {
            models.insert(DetailConectionsModel(with: movie), at: 2)
        }
    }
    
    private func getCast() {
        Api.shared.getCast(id: idMovie) { result in
            if let result = result {
                self.cast = result.cast
                self.models.append(DetailCastModel(with: self.movie))
                DispatchQueue.main.async {
                    self.detailView?.collection?.reloadData()
                }
            } else {
                self.cast = []
            }
        }
    }
    
    private func getVideos() {
        Api.shared.getVideoOfMovie(id: idMovie) { result in
            if let result = result, result.results.count > 0 {
                self.videos = result
            } else {
                self.videos = nil
            }
            DispatchQueue.main.async {
                self.detailView?.collection?.reloadData()
            }
        }
    }
    
    private func getSimilar() {
        Api.shared.getSimilar(id: idMovie) { result in
            if let result = result, result.results.count > 0 {
                self.similar = result.results
                self.models.append(DetailSimilarModel(with: self.movie))
            } else {
                self.similar = []
            }
            DispatchQueue.main.async {
                self.detailView?.collection?.reloadData()
            }
        }
    }
}
