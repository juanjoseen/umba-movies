//
//  DetailView.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailView: UIView {
    
    unowned var controller: DetailViewController!
    
    weak var collection: UICollectionView?
    private var lblTitle: UILabel?
    
    var title: String = "" {
        didSet {
            lblTitle?.text = title
        }
    }
    
    private weak var bannerAlert: BannerAlert?
    weak var loading: UIActivityIndicatorView?
    
    init(controller: DetailViewController) {
        self.controller = controller
        super.init(frame: .zero)
        backgroundColor = .primary
        
        let bar: UIView = UIFactory.getView(backgroundColor: .secondary)
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "UMBA Movies", color: .white, font: .title(), alignment: .center)
        self.lblTitle = lblTitle
        
        let btnBack: UIButton = UIFactory.getButton(image: UIImage(systemName: "chevron.backward"))
        btnBack.tintColor = .white
        btnBack.addTarget(self, action: #selector(actionBack), for: .touchUpInside)
        
        let loading: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        loading.translatesAutoresizingMaskIntoConstraints = false
        loading.startAnimating()
        self.loading = loading
        
        let collection: UICollectionView = UIFactory.getCollectionView()
        collection.register(DetailHeaderCell.self, forCellWithReuseIdentifier: DetailHeaderCell.reusableIdentifier())
        collection.register(DetailOverviewCell.self, forCellWithReuseIdentifier: DetailOverviewCell.reusableIdentifier())
        collection.register(DetailConectionsCell.self, forCellWithReuseIdentifier: DetailConectionsCell.reusableIdentifier())
        collection.register(DetailMoreInfoCell.self, forCellWithReuseIdentifier: DetailMoreInfoCell.reusableIdentifier())
        collection.register(DetailCastCell.self, forCellWithReuseIdentifier: DetailCastCell.reusableIdentifier())
        collection.register(DetailSimilarCell.self, forCellWithReuseIdentifier: DetailSimilarCell.reusableIdentifier())
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        let bannerAlert: BannerAlert = BannerAlert(message: "")
        bannerAlert.delegate = self
        self.bannerAlert = bannerAlert
        
        bar.addSubview(lblTitle)
        bar.addSubview(btnBack)
        
        addSubview(bar)
        addSubview(collection)
        addSubview(loading)
        addSubview(bannerAlert)
        
        NSLayoutConstraint.activate([
            
            bar.topAnchor.constraint(equalTo: topAnchor),
            bar.leadingAnchor.constraint(equalTo: leadingAnchor),
            bar.trailingAnchor.constraint(equalTo: trailingAnchor),
            bar.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 48),
            
            lblTitle.bottomAnchor.constraint(equalTo: bar.bottomAnchor, constant: .inversePadding / 2.0),
            lblTitle.leadingAnchor.constraint(equalTo: bar.leadingAnchor, constant: 56),
            lblTitle.trailingAnchor.constraint(equalTo: bar.trailingAnchor, constant: -56),
            lblTitle.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            
            btnBack.topAnchor.constraint(equalTo: lblTitle.topAnchor),
            btnBack.bottomAnchor.constraint(equalTo: lblTitle.bottomAnchor),
            btnBack.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding / 2.0),
            btnBack.widthAnchor.constraint(equalTo: btnBack.heightAnchor),
            
            collection.topAnchor.constraint(equalTo: bar.bottomAnchor),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor),
            collection.leadingAnchor.constraint(equalTo: leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            bannerAlert.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            bannerAlert.leadingAnchor.constraint(equalTo: leadingAnchor),
            bannerAlert.trailingAnchor.constraint(equalTo: trailingAnchor),
            bannerAlert.heightAnchor.constraint(equalToConstant: 100),
            
            loading.centerXAnchor.constraint(equalTo: centerXAnchor),
            loading.centerYAnchor.constraint(equalTo: centerYAnchor),
        ])
        
        hideAlert(animated: false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func fillDataWith(_ movie: Movie) {
        loading?.stopAnimating()
        collection?.reloadData()
    }
    
    func showError(_ message: String, of type: AlertType = .error) {
        self.bannerAlert?.message = message
        self.bannerAlert?.type = type
        self.showAlert()
    }
    
    @objc func actionBack() {
        controller.navigationController?.popViewController(animated: true)
    }
}

extension DetailView: UICollectionViewDelegate { }

extension DetailView: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        controller.models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model: DetailModel = controller.models[indexPath.row]
        let cell: DetailCell = collectionView.dequeueReusableCell(withReuseIdentifier: model.reusableIdentifier, for: indexPath) as! DetailCell
        cell.config(model: model.model, controller: controller)
        return cell
    }
}

extension DetailView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let model: DetailModel = controller.models[indexPath.row]
        let width: CGFloat = collectionView.frame.width
        var height: CGFloat = model.height
        
        if model.reusableIdentifier == DetailOverviewCell.reusableIdentifier() {
            if let overview: String = model.model.overview {
                height = min(overview.height(width: collectionView.frame.size.width - 32, font: .title()), 200)
            } else {
                height = 0
            }
        }
        
        return CGSize(width: width, height: height)
    }
}

extension DetailView: BannerAlertDelegate {
    func showAlert() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut) {
            self.bannerAlert?.transform = .identity
        } completion: { finished in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hideAlert()
            }
        }
    }
    
    func hideAlert(animated: Bool = true) {
        let moveUp: CGAffineTransform = CGAffineTransform(translationX: 0, y: -200)
        
        if animated {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
                self.bannerAlert?.transform = moveUp
            } completion: { finished in
            }
        } else {
            self.bannerAlert?.transform = moveUp
        }
    }
}
