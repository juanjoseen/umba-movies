//
//  DetailConectionsCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailConectionsCell: DetailCell {
    //weak var btnVideo: UIButton?
    weak var btnHomepage: UIButton?
    
    var movie: Movie!
    var controller: DetailViewController!
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        /*
        let btnVideo: UIButton = UIFactory.getButton(text: "Watch videos".localized, textColor: .white, font: .title(), backgroundColor: .systemTeal)
        btnVideo.layer.cornerRadius = 20
        btnVideo.addTarget(self, action: #selector(actionVideo), for: .touchUpInside)
        self.btnVideo = btnVideo
        */
        
        let btnHomepage: UIButton = UIFactory.getButton(text: "Homepage".localized, textColor: .white, font: .title(), backgroundColor: .systemBlue)
        btnHomepage.layer.cornerRadius = 20
        btnHomepage.addTarget(self, action: #selector(actionHomepage), for: .touchUpInside)
        self.btnHomepage = btnHomepage
        
        //addSubview(btnVideo)
        addSubview(btnHomepage)
        
        NSLayoutConstraint.activate([
            /*
            btnVideo.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            btnVideo.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            btnVideo.leadingAnchor.constraint(equalTo: centerXAnchor, constant: 8),
            btnVideo.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            */
            
            //btnHomepage.topAnchor.constraint(equalTo: btnVideo.topAnchor),
            //btnHomepage.bottomAnchor.constraint(equalTo: btnVideo.bottomAnchor),
            btnHomepage.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            btnHomepage.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            btnHomepage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            btnHomepage.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        self.movie = model
        self.controller = controller
        /*
        btnVideo?.isEnabled = controller.videos != nil
        btnVideo?.alpha = controller.videos != nil ? 1.0 : 0.6
         */
    }
    
    @objc func actionHomepage() {
        controller.goToHomepage()
    }
    
    @objc func actionVideo() {
        controller.goToVideos()
    }
}
