//
//  DetailMoreInfoCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailMoreInfoCell: DetailCell {
    var movie: Movie!
    var controller: DetailViewController!
    
    private let padding: CGFloat = 16
    
    weak var lblScore: UILabel?
    weak var lblVotes: UILabel?
    weak var lblRuntime: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let lblRuntime: UILabel = UIFactory.getLabel(text: "Runtime:".localized, font: .title())
        self.lblRuntime = lblRuntime
        
        let lblScore: UILabel = UIFactory.getLabel(text: "Score:".localized, font: .title())
        self.lblScore = lblScore
        
        let lblVotes: UILabel = UIFactory.getLabel(text: "Votes:".localized, font: .title())
        self.lblVotes = lblVotes
        
        addSubview(lblRuntime)
        addSubview(lblScore)
        addSubview(lblVotes)
        
        NSLayoutConstraint.activate([
            
            lblRuntime.heightAnchor.constraint(equalToConstant: 24),
            lblRuntime.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            lblRuntime.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblRuntime.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            
            lblScore.topAnchor.constraint(equalTo: lblRuntime.bottomAnchor, constant: padding),
            lblScore.leadingAnchor.constraint(equalTo: lblRuntime.leadingAnchor),
            lblScore.trailingAnchor.constraint(equalTo: lblRuntime.trailingAnchor),
            lblScore.heightAnchor.constraint(equalTo: lblRuntime.heightAnchor),
            
            lblVotes.topAnchor.constraint(equalTo: lblScore.bottomAnchor, constant: padding),
            lblVotes.leadingAnchor.constraint(equalTo: lblScore.leadingAnchor),
            lblVotes.trailingAnchor.constraint(equalTo: lblScore.trailingAnchor),
            lblVotes.heightAnchor.constraint(equalTo: lblScore.heightAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        self.movie = model
        self.controller = controller
        if let runtime = model.runtime {
            lblRuntime?.text = String(format: "Runtime: %d minutes".localized, runtime)
        } else {
            lblRuntime?.text = "Runtime: -".localized
        }
        lblScore?.text = String(format: "Score: %.2lf".localized, model.vote_average)
        lblVotes?.text = String(format: "Votes: %d".localized, model.vote_count)
    }
}
