//
//  DetailCastCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailCastCell: DetailCell {
    
    weak var collection: UICollectionView?
    
    var controller: DetailViewController!
    var model: Movie!
    
    /*
    var controller2: DetailShowController!
    var model2: Show!
     */
    
    private var isMovie: Bool = true
    
    private let padding: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let lblCast: UILabel = UIFactory.getLabel(text: "Cast".localized, font: .title())
        
        let flow: UICollectionViewFlowLayout = UIFactory.getCollectionLayout(direction: .horizontal, lineSpacing: 16, headerSize: CGSize(width: 17, height: 0))
        
        let collection: UICollectionView = UIFactory.getCollectionView(layout: flow)
        collection.register(CastCell.self, forCellWithReuseIdentifier: CastCell.reusableIdentifier())
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        addSubview(lblCast)
        addSubview(collection)
        
        NSLayoutConstraint.activate([
            lblCast.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            lblCast.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            
            collection.topAnchor.constraint(equalTo: lblCast.bottomAnchor, constant: padding / 2.0),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor),
            collection.leadingAnchor.constraint(equalTo: leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        isMovie = true
        self.model = model
        self.controller = controller
    }
    
    /*
    override func config(model: Show, controller: DetailShowController) {
        isMovie = false
        self.model2 = model
        self.controller2 = controller
    }
     */
}

extension DetailCastCell: UICollectionViewDelegate {}

extension DetailCastCell: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.cast.count // isMovie ? controller.cast.count : controller2.cast.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CastCell = collectionView.dequeueReusableCell(withReuseIdentifier: CastCell.reusableIdentifier(), for: indexPath) as! CastCell
        //if isMovie {
            cell.config(model: controller.cast[indexPath.row])
        //} else {
        //    cell.config(model: controller2.cast[indexPath.row])
        //}
        return cell
    }
}

extension DetailCastCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 120
        let height: CGFloat = collectionView.frame.height - 10
        return CGSize(width: width, height: height)
    }
}
