//
//  DetailCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailCell: UICollectionViewCell {
    required init?(coder:NSCoder) {
        fatalError()
    }
    
    override init(frame:CGRect) {
        super.init(frame:frame)
        clipsToBounds = true
        backgroundColor = .clear
    }
    
    func config(model: Movie, controller: DetailViewController){}
    //func config(model: Show, controller: DetailShowController){}
}
