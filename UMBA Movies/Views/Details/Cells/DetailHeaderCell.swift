//
//  DetailHeaderCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailHeaderCell: DetailCell {
    weak var imgBackground: UIImageView?
    weak var imgPoster: UIImageView?
    weak var lblOriginalTitle: UILabel?
    weak var lblReleaseDate: UILabel?
    weak var lblGenresList: UILabel?
    
    private let padding: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let imgBackground: UIImageView = UIFactory.getImageView(contentMode: .scaleAspectFill)
        self.imgBackground = imgBackground
        let shadow: UIView = UIFactory.getView(backgroundColor: UIColor(white: 0, alpha: 0.75))
        imgBackground.addSubview(shadow)
        
        let imgPoster: UIImageView = UIFactory.getImageView(contentMode: .scaleAspectFill)
        imgPoster.backgroundColor = .white
        self.imgPoster = imgPoster
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "Original title:".localized, color: .white, font: .text())
        let lblOriginalTitle: UILabel = UIFactory.getLabel(color: .white, font: .title())
        lblOriginalTitle.numberOfLines = 2
        self.lblOriginalTitle = lblOriginalTitle
        
        let lblRelease: UILabel = UIFactory.getLabel(text: "release date:".localized, color: .white, font: .text())
        let lblReleaseDate: UILabel = UIFactory.getLabel(color: .white, font: .title())
        self.lblReleaseDate = lblReleaseDate
        
        let lblGenres: UILabel = UIFactory.getLabel(text: "Genres:".localized, color: .white, font: .text())
        let lblGenresList: UILabel = UIFactory.getLabel(color: .white, font: .title())
        self.lblGenresList = lblGenresList
        
        addSubview(imgBackground)
        addSubview(imgPoster)
        addSubview(lblTitle)
        addSubview(lblOriginalTitle)
        addSubview(lblRelease)
        addSubview(lblReleaseDate)
        addSubview(lblGenres)
        addSubview(lblGenresList)
        
        NSLayoutConstraint.activate([
            imgBackground.topAnchor.constraint(equalTo: topAnchor),
            imgBackground.bottomAnchor.constraint(equalTo: bottomAnchor),
            imgBackground.leadingAnchor.constraint(equalTo: leadingAnchor),
            imgBackground.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            shadow.topAnchor.constraint(equalTo: imgBackground.topAnchor),
            shadow.bottomAnchor.constraint(equalTo: imgBackground.bottomAnchor),
            shadow.leadingAnchor.constraint(equalTo: imgBackground.leadingAnchor),
            shadow.trailingAnchor.constraint(equalTo: imgBackground.trailingAnchor),
            
            imgPoster.heightAnchor.constraint(equalToConstant: 200),
            imgPoster.centerYAnchor.constraint(equalTo: centerYAnchor),
            imgPoster.widthAnchor.constraint(equalTo: imgPoster.heightAnchor, multiplier: 0.66),
            imgPoster.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            
            lblTitle.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            lblTitle.leadingAnchor.constraint(equalTo: imgPoster.trailingAnchor, constant: padding),
            lblTitle.heightAnchor.constraint(equalToConstant: 20),
            
            lblOriginalTitle.topAnchor.constraint(equalTo: lblTitle.bottomAnchor),
            lblOriginalTitle.leadingAnchor.constraint(equalTo: lblTitle.leadingAnchor, constant: padding),
            lblOriginalTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding * 2),
            
            lblRelease.topAnchor.constraint(equalTo: lblOriginalTitle.bottomAnchor, constant: padding),
            lblRelease.leadingAnchor.constraint(equalTo: lblTitle.leadingAnchor),
            lblRelease.heightAnchor.constraint(equalTo: lblTitle.heightAnchor),
            
            lblReleaseDate.topAnchor.constraint(equalTo: lblRelease.bottomAnchor),
            lblReleaseDate.leadingAnchor.constraint(equalTo: lblOriginalTitle.leadingAnchor),
            lblReleaseDate.trailingAnchor.constraint(equalTo: lblOriginalTitle.trailingAnchor),
            
            lblGenres.topAnchor.constraint(equalTo: lblReleaseDate.bottomAnchor, constant: padding),
            lblGenres.leadingAnchor.constraint(equalTo: lblRelease.leadingAnchor),
            lblGenres.heightAnchor.constraint(equalTo: lblRelease.heightAnchor),
            
            lblGenresList.topAnchor.constraint(equalTo: lblGenres.bottomAnchor),
            lblGenresList.leadingAnchor.constraint(equalTo: lblReleaseDate.leadingAnchor),
            lblGenresList.trailingAnchor.constraint(equalTo: lblReleaseDate.trailingAnchor),
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        if let path = model.backdrop_path {
            let url: String = String(format: "https://image.tmdb.org/t/p/w500/%@", path)
            imgBackground?.cacheImage(urlString: url)
        } else {
            imgBackground?.image = nil
        }
        
        let url: String = String(format: "https://image.tmdb.org/t/p/w200/%@", model.poster_path)
        imgPoster?.cacheImage(urlString: url)
        
        lblOriginalTitle?.text = model.original_title
        lblReleaseDate?.text = model.release_date
        lblGenresList?.text = model.genres.map( { $0.name } ).joined(separator: ", ")
        
    }
}
