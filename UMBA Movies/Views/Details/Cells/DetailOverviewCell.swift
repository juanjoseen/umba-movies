//
//  DetailOverviewCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailOverviewCell: DetailCell {
    
    private let padding: CGFloat = 16
    weak var lblOverview: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "Overview".localized, font: .title(), alignment: .center)
        
        let lblOverview: UILabel = UIFactory.getLabel(font: .text())
        lblOverview.numberOfLines = 8
        
        self.lblOverview = lblOverview
        
        addSubview(lblTitle)
        addSubview(lblOverview)
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: topAnchor),
            lblTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            
            lblOverview.topAnchor.constraint(equalTo: lblTitle.bottomAnchor),
            lblOverview.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            lblOverview.leadingAnchor.constraint(equalTo: lblTitle.leadingAnchor),
            lblOverview.trailingAnchor.constraint(equalTo: lblTitle.trailingAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        lblOverview?.text = model.overview
    }
    
    /*
    override func config(model: Show, controller: DetailViewController) {
        if model.overview.trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            lblOverview?.text = model.overview.trimmingCharacters(in: .whitespacesAndNewlines)
        } else {
            lblOverview?.text = "No description".localized
        }
    }
     */
}
