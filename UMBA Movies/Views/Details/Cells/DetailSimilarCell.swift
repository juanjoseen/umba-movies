//
//  DetailSimilarCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailSimilarCell: DetailCell {
    
    var controller: DetailViewController!
    var model: Movie!
    //var controller2: DetailShowController!
    //var model2: Show!
    
    private var isMovie: Bool = true
    
    private weak var collection: UICollectionView?
    private weak var lblTitle: UILabel?
    
    private let padding: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "Similar Movies".localized, font: .title())
        self.lblTitle = lblTitle
        
        let layout: UICollectionViewFlowLayout = UIFactory.getCollectionLayout(direction: .horizontal)
        let collection: UICollectionView = UIFactory.getCollectionView(layout: layout)
        collection.register(SimilarCell.self, forCellWithReuseIdentifier: SimilarCell.reusableIdentifier())
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        addSubview(lblTitle)
        addSubview(collection)
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: topAnchor, constant: padding * 2),
            lblTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            
            collection.leadingAnchor.constraint(equalTo: leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: trailingAnchor),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            collection.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: padding),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func config(model: Movie, controller: DetailViewController) {
        self.model = model
        self.controller = controller
        isMovie = true
        lblTitle?.text = "Similar Movies".localized
    }
    
    /*
    override func config(model: Show, controller: DetailShowController) {
        self.model2 = model
        self.controller2 = controller
        isMovie = false
        lblTitle?.text = "Similar Shows".localized
    }
     */
}

extension DetailSimilarCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.similar.count // isMovie ? controller.similar.count : controller2.similar.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //if isMovie {
            let model: MoviePreview = controller.similar[indexPath.row]
            let cell: SimilarCell = collectionView.dequeueReusableCell(withReuseIdentifier: SimilarCell.reusableIdentifier(), for: indexPath) as! SimilarCell
            cell.config(model: model, controller: controller)
            return cell
        /*
        } else {
            let model: ShowPreview = controller2.similar[indexPath.row]
            let cell: SimilarCell = collectionView.dequeueReusableCell(withReuseIdentifier: SimilarCell.reusableIdentifier(), for: indexPath) as! SimilarCell
            cell.config(model: model, controller: controller2)
            return cell
        }
         */
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = collectionView.frame.height
        let width: CGFloat = height * 0.61
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //if isMovie {
            let model: MoviePreview = controller.similar[indexPath.row]
            controller.showDetailsOf(model)
        //} else {
        //    let model: ShowPreview = controller2.similar[indexPath.row]
        //    controller2.showDetailsOf(model)
        //}
    }
}
