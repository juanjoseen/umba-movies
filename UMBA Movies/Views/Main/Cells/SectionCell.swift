//
//  SectionCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class SectionCell: UICollectionViewCell {
    
    private var items: [Preview] = []
    
    private weak var lblTitle: UILabel?
    private weak var collection: UICollectionView?
    private unowned var controller: ViewController!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let lblTitle: UILabel = UIFactory.getLabel(font: .subtitle(), alignment: .left)
        self.lblTitle = lblTitle
        
        let flow: UICollectionViewFlowLayout = UIFactory.getCollectionLayout(direction: .horizontal, lineSpacing: 8)
        let collection: UICollectionView = UIFactory.getCollectionView(layout: flow)
        collection.register(MovieCell.self, forCellWithReuseIdentifier: MovieCell.reusableIdentifier())
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        addSubview(lblTitle)
        addSubview(collection)
        
        NSLayoutConstraint.activate([
            lblTitle.topAnchor.constraint(equalTo: topAnchor, constant: .padding / 2.0),
            lblTitle.leadingAnchor.constraint(equalTo: leadingAnchor, constant: .padding),
            lblTitle.trailingAnchor.constraint(equalTo: trailingAnchor, constant: .inversePadding),
            
            collection.topAnchor.constraint(equalTo: lblTitle.bottomAnchor, constant: .padding / 2.0),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor, constant: .inversePadding),
            collection.leadingAnchor.constraint(equalTo: leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(title: String, items: [Preview], controller: ViewController) {
        self.controller = controller
        self.items = items
        
        lblTitle?.text = title
        collection?.reloadData()
    }
}

extension SectionCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let model: Preview = items[indexPath.row]
        controller.showDetailsOf(model)
    }
}

extension SectionCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: MovieCell = collectionView.dequeueReusableCell(withReuseIdentifier: MovieCell.reusableIdentifier(), for: indexPath) as! MovieCell
        
        let model: Preview = items[indexPath.row]
        cell.config(model: model)
        
        return cell
    }
}

extension SectionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = 120
        let height: CGFloat = collectionView.frame.height - 16
        return CGSize(width: width, height: height)
    }
}
