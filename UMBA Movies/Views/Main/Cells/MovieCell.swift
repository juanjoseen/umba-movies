//
//  MovieCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    private weak var lblName: UILabel?
    private weak var imgPoster: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let loading: UIActivityIndicatorView = UIActivityIndicatorView(style: .large)
        loading.startAnimating()
        loading.translatesAutoresizingMaskIntoConstraints = false
        
        let imgPoster: UIImageView = UIFactory.getImageView(contentMode: .scaleAspectFill)
        self.imgPoster = imgPoster
        
        let lblName: UILabel = UIFactory.getLabel(font: .subtitle(), alignment: .center)
        self.lblName = lblName
        
        addSubview(loading)
        addSubview(imgPoster)
        addSubview(lblName)
        
        NSLayoutConstraint.activate([
            lblName.heightAnchor.constraint(equalToConstant: 48),
            lblName.bottomAnchor.constraint(equalTo: bottomAnchor),
            lblName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            lblName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            
            imgPoster.topAnchor.constraint(equalTo: topAnchor),
            imgPoster.bottomAnchor.constraint(equalTo: lblName.topAnchor, constant: -8),
            imgPoster.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16),
            imgPoster.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16),
            
            loading.centerXAnchor.constraint(equalTo: imgPoster.centerXAnchor),
            loading.centerYAnchor.constraint(equalTo: imgPoster.centerYAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(model: Preview) {
        if model is MoviePreview {
            let movie: MoviePreview = model as! MoviePreview
            lblName?.text = movie.title
        } else {
            let show: ShowPreview = model as! ShowPreview
            lblName?.text = show.name
        }
        
        if let path = model.poster_path {
            let url: String = String(format: "https://image.tmdb.org/t/p/w200/%@", path)
            imgPoster?.cacheImage(urlString: url)
        } else {
            imgPoster?.image = nil
        }
    }
}
