//
//  VMain.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class VMain: UIView {
    
    unowned var controller: ViewController!
    
    weak var collection: UICollectionView?
    weak var bannerAlert: BannerAlert?
    
    init(controller: ViewController) {
        super.init(frame: .zero)
        self.controller = controller
        
        backgroundColor = .primary
        
        let bar: UIView = UIFactory.getView(backgroundColor: .secondary)
        
        let lblTitle: UILabel = UIFactory.getLabel(text: "UMBA Movies", color: .white, font: .title(), alignment: .center)
        
        let collection: UICollectionView = UIFactory.getCollectionView()
        collection.register(SectionCell.self, forCellWithReuseIdentifier: SectionCell.reusableIdentifier())
        
        collection.delegate = self
        collection.dataSource = self
        self.collection = collection
        
        let bannerAlert: BannerAlert = BannerAlert(message: "")
        bannerAlert.delegate = self
        self.bannerAlert = bannerAlert
        
        bar.addSubview(lblTitle)
        
        addSubview(bar)
        addSubview(collection)
        addSubview(bannerAlert)
        
        NSLayoutConstraint.activate([
            bar.topAnchor.constraint(equalTo: topAnchor),
            bar.leadingAnchor.constraint(equalTo: leadingAnchor),
            bar.trailingAnchor.constraint(equalTo: trailingAnchor),
            bar.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 48),
            
            lblTitle.bottomAnchor.constraint(equalTo: bar.bottomAnchor, constant: .inversePadding),
            lblTitle.leadingAnchor.constraint(equalTo: bar.leadingAnchor, constant: 56),
            lblTitle.trailingAnchor.constraint(equalTo: bar.trailingAnchor, constant: -56),
            
            collection.topAnchor.constraint(equalTo: bar.bottomAnchor),
            collection.leadingAnchor.constraint(equalTo: leadingAnchor),
            collection.trailingAnchor.constraint(equalTo: trailingAnchor),
            collection.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            bannerAlert.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            bannerAlert.leadingAnchor.constraint(equalTo: leadingAnchor),
            bannerAlert.trailingAnchor.constraint(equalTo: trailingAnchor),
            bannerAlert.heightAnchor.constraint(equalToConstant: 100),
        ])
        
        hideAlert(animated: false)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func showError(_ message: String, of type: AlertType = .error) {
        self.bannerAlert?.message = message
        self.bannerAlert?.type = type
        self.showAlert()
    }
    
}

extension VMain: UICollectionViewDelegate {
    
}

extension VMain: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return controller.sections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: SectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: SectionCell.reusableIdentifier(), for: indexPath) as! SectionCell
        
        let model: SectionModel = controller.sections[indexPath.row]
        cell.configure(title: model.name, items: model.items, controller: controller)
        
        return cell
    }
    
    
}

extension VMain: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = collectionView.frame.width
        let height: CGFloat = 250
        
        return CGSize(width: width, height: height)
    }
}

extension VMain: BannerAlertDelegate {
    func showAlert() {
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseOut) {
            self.bannerAlert?.transform = .identity
        } completion: { finished in
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.hideAlert()
            }
        }
    }
    
    func hideAlert(animated: Bool = true) {
        
        let moveUp: CGAffineTransform = CGAffineTransform(translationX: 0, y: -200)
        
        if animated {
            UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseIn) {
                self.bannerAlert?.transform = moveUp
            } completion: { finished in
            }

        } else {
            self.bannerAlert?.transform = moveUp
        }
    }
}
