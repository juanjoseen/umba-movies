//
//  Preview.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

protocol Preview {
    var id: Int { get }
    var poster_path: String? { get }
    var overview: String { get }
    var genre_ids: [Int] { get }
    var original_language: String { get }
    var backdrop_path: String? { get }
    var popularity: Double { get }
    var vote_count: Int { get }
    var vote_average: Double { get }
}
