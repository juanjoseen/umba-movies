//
//  ShowPreview.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

class ShowPreview: Preview, Codable {
    let id: Int
    let poster_path: String?
    let overview: String
    let genre_ids: [Int]
    let original_name: String
    let original_language: String
    let name: String
    let backdrop_path: String?
    let popularity: Double
    let vote_count: Int
    let vote_average: Double
}
