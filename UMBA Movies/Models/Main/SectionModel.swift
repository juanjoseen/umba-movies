//
//  SectionModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

class SectionModel {
    let name: String
    var items: [Preview]
    
    init(name: String, items: [Preview] = []) {
        self.name = name
        self.items = items
    }
}
