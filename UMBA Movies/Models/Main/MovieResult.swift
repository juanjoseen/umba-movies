//
//  MovieResult.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

struct MovieResult: Codable {
    let page: Int
    let results: [MoviePreview]
    let total_pages: Int
    let total_results: Int
}
