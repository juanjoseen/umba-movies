//
//  ShowResult.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

struct ShowResult: Codable {
    let page: Int
    let results: [ShowPreview]
    let total_pages: Int
    let total_results: Int
}
