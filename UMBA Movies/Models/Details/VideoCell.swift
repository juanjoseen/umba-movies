//
//  VideoCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class VideoCell: UICollectionViewCell {
    
    private let padding: CGFloat = 16
    
    private weak var lblName: UILabel?
    private weak var lblPusblised: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let line: UIView = UIFactory.getView(backgroundColor: UIColor(white: 1, alpha: 0.5))
        
        let lblName: UILabel = UIFactory.getLabel(font: .largeTitle())
        lblName.numberOfLines = 2
        self.lblName = lblName
        
        let lblPusblised: UILabel = UIFactory.getLabel(font: .smallText(9))
        self.lblPusblised = lblPusblised
        
        addSubview(line)
        addSubview(lblName)
        addSubview(lblPusblised)
        
        NSLayoutConstraint.activate([
            
            lblName.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            lblName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            lblName.bottomAnchor.constraint(equalTo: lblPusblised.topAnchor, constant: -padding / 2),
            
            lblPusblised.heightAnchor.constraint(equalToConstant: 13),
            lblPusblised.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding / 2),
            lblPusblised.leadingAnchor.constraint(equalTo: lblName.leadingAnchor),
            lblPusblised.trailingAnchor.constraint(equalTo: lblName.trailingAnchor),
            
            line.heightAnchor.constraint(equalToConstant: 1),
            line.bottomAnchor.constraint(equalTo: bottomAnchor),
            line.leadingAnchor.constraint(equalTo: leadingAnchor),
            line.trailingAnchor.constraint(equalTo: trailingAnchor),
            
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(model: ResultVideo) {
        lblName?.text = model.name
        lblPusblised?.text = String(format: "Published at: %@".localized, model.published_at)
    }
}
