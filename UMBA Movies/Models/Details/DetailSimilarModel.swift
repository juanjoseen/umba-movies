//
//  DetailSimilarModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailSimilarModel: DetailModel {
    let kCellHeight: CGFloat = 280
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailSimilarCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
