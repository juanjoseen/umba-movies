//
//  DetailOverviewModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailOverviewModel: DetailModel {
    let kCellHeight: CGFloat = 0
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailOverviewCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
