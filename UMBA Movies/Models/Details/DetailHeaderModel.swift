//
//  DetailHeaderModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailHeaderModel: DetailModel {
    let kCellHeight: CGFloat = 250
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailHeaderCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
