//
//  DetailConectionsModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailConectionsModel: DetailModel {
    let kCellHeight: CGFloat = 56
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailConectionsCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
