//
//  DetailMoreInfoModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailMoreInfoModel: DetailModel {
    let kCellHeight: CGFloat = 128
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailMoreInfoCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
