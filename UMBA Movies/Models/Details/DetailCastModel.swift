//
//  DetailCastModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailCastModel: DetailModel {
    let kCellHeight: CGFloat = 280
    
    init(with model: Movie) {
        super.init(reusableIdentifier: DetailCastCell.reusableIdentifier(), height: kCellHeight, model: model)
    }
}
