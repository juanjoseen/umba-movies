//
//  Video.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

class Video: Codable {
    let id: Int
    let results: [ResultVideo]
}

struct ResultVideo: Codable {
    let name: String
    let key: String
    let site: String
    let size: Int
    let type: String
    let official: Bool
    let published_at: String
    let id: String
}
