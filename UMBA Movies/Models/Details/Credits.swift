//
//  Credits.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

struct Credits: Codable {
    let id: Int
    let cast: [Cast]
}

struct Cast: Codable {
    let gender: Int?
    let id: Int
    let known_for_department: String
    let name: String
    let original_name: String
    let profile_path: String?
    let character: String
    let credit_id: String
    let order: Int
}
