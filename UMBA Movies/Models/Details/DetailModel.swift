//
//  DetailModel.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class DetailModel {
    let reusableIdentifier: String
    let height: CGFloat
    var model: Movie
    
    init(reusableIdentifier: String, height: CGFloat, model: Movie) {
        self.reusableIdentifier = reusableIdentifier
        self.height = height
        self.model = model
    }
}
