//
//  SimilarCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class SimilarCell: UICollectionViewCell {
    
    private let padding: CGFloat = 16
    
    private weak var lblName: UILabel?
    private weak var imgPoster: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        let lblName: UILabel = UIFactory.getLabel(font: .smallText(), alignment: .center)
        lblName.numberOfLines = 3
        self.lblName = lblName
        
        let imgPoster: UIImageView = UIFactory.getImageView()
        self.imgPoster = imgPoster
        
        addSubview(imgPoster)
        addSubview(lblName)
        
        NSLayoutConstraint.activate([
            imgPoster.topAnchor.constraint(equalTo: topAnchor),
            imgPoster.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            imgPoster.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            imgPoster.heightAnchor.constraint(equalTo: imgPoster.widthAnchor, multiplier: 1.5),
            
            lblName.topAnchor.constraint(equalTo: imgPoster.bottomAnchor),
            lblName.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding / 2.0),
            lblName.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(model: MoviePreview, controller: DetailViewController) {
        lblName?.text = model.title
        if let path = model.poster_path {
            let url: String = String(format: "https://image.tmdb.org/t/p/w200/%@", path)
            imgPoster?.cacheImage(urlString: url)
        } else {
            imgPoster?.image = nil
        }
    }
    /*
    func config(model: ShowPreview, controller: DetailShowController) {
        lblName?.text = model.name
        if let path = model.poster_path {
            let url: String = String(format: "https://image.tmdb.org/t/p/w200/%@", path)
            imgPoster?.cacheImage(urlString: url)
        } else {
            imgPoster?.image = nil
        }
    }
     */
}
