//
//  CastCell.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import UIKit

class CastCell: UICollectionViewCell {
    
    private let padding: CGFloat = 8
    
    weak var lblCharacter: UILabel?
    weak var lblName: UILabel?
    weak var imgCast: UIImageView?
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        backgroundColor = .primary
        layer.shadowOpacity = 1.0
        layer.shadowRadius = 3
        layer.shadowColor = UIColor.textColor.cgColor
        layer.shadowOffset = .zero
        
        let imgCast: UIImageView = UIFactory.getImageView(contentMode: .scaleAspectFill)
        self.imgCast = imgCast
        
        let lblName: UILabel = UIFactory.getLabel(font: .subtitle(12), alignment: .center)
        self.lblName = lblName
        
        let lblCharacter: UILabel = UIFactory.getLabel(font: .smallText(10), alignment: .center)
        self.lblCharacter = lblCharacter
        
        addSubview(imgCast)
        addSubview(lblName)
        addSubview(lblCharacter)
        
        NSLayoutConstraint.activate([
            imgCast.heightAnchor.constraint(equalToConstant: 145),
            imgCast.topAnchor.constraint(equalTo: topAnchor, constant: padding),
            imgCast.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            imgCast.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
            
            lblName.bottomAnchor.constraint(equalTo: lblCharacter.topAnchor, constant: -padding),
            lblName.leadingAnchor.constraint(equalTo: lblCharacter.leadingAnchor),
            lblName.trailingAnchor.constraint(equalTo: lblCharacter.trailingAnchor),
            
            lblCharacter.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -padding),
            lblCharacter.leadingAnchor.constraint(equalTo: leadingAnchor, constant: padding),
            lblCharacter.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -padding),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(model: Cast) {
        if let profile = model.profile_path {
            let url: String = String(format: "https://image.tmdb.org/t/p/w200/%@", profile)
            imgCast?.cacheImage(urlString: url)
        } else {
            imgCast?.image = nil
        }
        
        lblName?.text = model.name
        lblCharacter?.text = model.character
    }
}
