//
//  Movie.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

struct Movie: Codable {
    let adult: Bool
    let backdrop_path: String?
    let budget: Int
    let genres: [Genre]
    let homepage: String?
    let id: Int
    let imdb_id: String?
    let original_language: String
    let original_title: String
    let overview: String?
    let popularity: Double
    let poster_path: String
    let release_date: String
    let revenue: Int
    let runtime: Int?
    let status: String
    let tagline: String?
    let title: String
    let video: Bool
    let vote_average: Double
    let vote_count: Int
    
    enum CodingKeys: String, CodingKey {
        case adult, backdrop_path, budget, genres, homepage, id, imdb_id, original_language, original_title, overview, popularity, poster_path, release_date, revenue, runtime, status, tagline, title, video, vote_average, vote_count
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        adult = try values.decode(Bool.self, forKey: .adult)
        backdrop_path = try values.decode(String.self, forKey: .backdrop_path)
        budget = try values.decode(Int.self, forKey: .budget)
        genres = try values.decode([Genre].self, forKey: .genres)
        homepage = try values.decode(String.self, forKey: .homepage)
        id = try values.decode(Int.self, forKey: .id)
        imdb_id = try values.decode(String.self, forKey: .imdb_id)
        original_language = try values.decode(String.self, forKey: .original_language)
        original_title = try values.decode(String.self, forKey: .original_title)
        overview = try values.decode(String.self, forKey: .overview)
        popularity = try values.decode(Double.self, forKey: .popularity)
        poster_path = try values.decode(String.self, forKey: .poster_path)
        release_date = try values.decode(String.self, forKey: .release_date)
        revenue = try values.decode(Int.self, forKey: .revenue)
        runtime = try values.decode(Int.self, forKey: .runtime)
        status = try values.decode(String.self, forKey: .status)
        tagline = try values.decode(String.self, forKey: .tagline)
        title = try values.decode(String.self, forKey: .title)
        video = try values.decode(Bool.self, forKey: .video)
        vote_average = try values.decode(Double.self, forKey: .vote_average)
        vote_count = try values.decode(Int.self, forKey: .runtime)
    }
}
