//
//  Genre.swift
//  UMBA Movies
//
//  Created by Juan Jose Elias Navarro on 15/12/21.
//

import Foundation

struct Genre: Codable {
    let id: Int
    let name: String
}

struct GenreList: Codable {
    let genres: [Genre]
}
